package br.com.bueno.dribbblesimpleapp.service;


import br.com.bueno.dribbblesimpleapp.component.PopularShot;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by Rafael on 10/06/2015.
 */
public interface RestService {
    @GET("/shots/popular")
    PopularShot retrieveShot(@Query("page") int page, @Query("per_page") int per_page);
}
