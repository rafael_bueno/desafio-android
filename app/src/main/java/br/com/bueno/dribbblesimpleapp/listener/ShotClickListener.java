package br.com.bueno.dribbblesimpleapp.listener;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import br.com.bueno.dribbblesimpleapp.activity.DetailActivity_;
import br.com.bueno.dribbblesimpleapp.component.Shot;

/**
 * Created by Rafael on 10/06/2015.
 */
public class ShotClickListener implements View.OnClickListener {

    private Context mContext;

    public ShotClickListener(Context context) {
        this.mContext = context;
    }

    @Override
    public void onClick(View v) {

        if (v.getTag() instanceof Shot) {
            Shot shot = (Shot) v.getTag();
            Intent intent = new Intent(mContext, DetailActivity_.class);
            intent.putExtra("SHOT", shot);
            mContext.startActivity(intent);
        }
    }
}
