package br.com.bueno.dribbblesimpleapp.activity;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.SystemService;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import br.com.bueno.dribbblesimpleapp.R;
import br.com.bueno.dribbblesimpleapp.adapter.ShotListAdapter;
import br.com.bueno.dribbblesimpleapp.component.PopularShot;
import br.com.bueno.dribbblesimpleapp.service.RestClient;
import br.com.bueno.dribbblesimpleapp.service.RestService;

@EActivity(R.layout.activity_dribbble)
public class DribbbleActivity extends BaseActivity {


    Integer mItemPerPage = 15;
    @ViewById(R.id.shot_list)
    RecyclerView mShotList;
    ShotListAdapter mAdapter;
    RestService mService;

    @ViewById(R.id.errorText)
    TextView mErrorText;
    @ViewById(R.id.button_try_again)
    Button mButtonTryAgain;

    int mPageCount = 1;
    Integer mMaxPages;
    boolean mLoading;

    @SystemService
    ConnectivityManager mConnectivityManager;

    @AfterViews
    void afterViews() {

        if (mShotList.getLayoutManager() == null) {
            final LinearLayoutManager manager = new LinearLayoutManager(this);
            manager.setOrientation(LinearLayoutManager.VERTICAL);
            mShotList.setLayoutManager(manager);

            mShotList.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                    int visibleItemCount = manager.getChildCount();
                    int totalItemCount = manager.getItemCount();
                    int pastVisiblesItems = manager.findFirstVisibleItemPosition();

                    if (!mLoading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount && mPageCount < mMaxPages) {
                            if (hasConnection()) {
                                showProgressDialog();
                                mLoading = true;
                                mPageCount++;
                                searchShots(mPageCount);
                            }
                        }
                    }
                }
            });
        }

        showProgressDialog();

        if (hasConnection()) {
            //inicializando o adapter da lista de shots
            mAdapter = new ShotListAdapter(this);
            //inicializa o rest service
            mService = RestClient.getService();
            //inicializa os itens da primeira pagina
            searchShots(1);
            mShotList.setAdapter(mAdapter);
        } else {
            mErrorText.setVisibility(View.VISIBLE);
            mButtonTryAgain.setVisibility(View.VISIBLE);
            dismissProgressDialog();
        }
    }

    @Background
    void searchShots(int page) {
        PopularShot popularShot = mService.retrieveShot(page, mItemPerPage);
        mItemPerPage = popularShot.getItemPerPage();
        mMaxPages = popularShot.getPageCount();
        dismissProgressDialog();
        updateList(popularShot);
        mLoading = false;
    }

    @Click(R.id.button_try_again)
    void tryAgain() {
        mErrorText.setVisibility(View.GONE);
        mButtonTryAgain.setVisibility(View.GONE);
        afterViews();
    }

    @UiThread
    void updateList(PopularShot popularShot) {
        mAdapter.newPage(popularShot.getShots());
    }


    public boolean hasConnection() {
        NetworkInfo netInfo = mConnectivityManager.getActiveNetworkInfo();

        if (netInfo == null)
            return false;
        if (!netInfo.isConnected())
            return false;
        if (!netInfo.isAvailable())
            return false;

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_dribbble, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:

                if (hasConnection()) {
                    afterViews();
                } else {
                    Toast.makeText(this, getString(R.string.no_connection), Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.action_about:
                Toast.makeText(this, getString(R.string.about), Toast.LENGTH_LONG).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}