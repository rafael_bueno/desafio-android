package br.com.bueno.dribbblesimpleapp.component;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Rafael on 10/06/2015.
 */
public class PopularShot implements Serializable {

    @SerializedName("shots")
    private ArrayList<Shot> mShots;
    @SerializedName("per_page")
    private Integer mItemPerPage;
    @SerializedName("pages")
    private Integer mPageCount;

    public ArrayList<Shot> getShots() {
        return mShots;
    }

    public void setShots(ArrayList<Shot> shots) {
        this.mShots = shots;
    }

    public Integer getItemPerPage() {
        return mItemPerPage;
    }

    public void setItemPerPage(Integer itemPerPage) {
        this.mItemPerPage = itemPerPage;
    }

    public Integer getPageCount() {
        return mPageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.mPageCount = pageCount;
    }
}
