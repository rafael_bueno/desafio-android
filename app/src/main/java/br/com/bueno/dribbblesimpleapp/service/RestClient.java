package br.com.bueno.dribbblesimpleapp.service;

import retrofit.RestAdapter;

/**
 * Created by Rafael on 10/06/2015.
 */
public class RestClient {

    private static final String URL = "http://api.dribbble.com";

    public static RestService getService() {
        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(URL).build();
        return adapter.create(RestService.class);
    }
}
