package br.com.bueno.dribbblesimpleapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.bueno.dribbblesimpleapp.R;
import br.com.bueno.dribbblesimpleapp.component.Shot;
import br.com.bueno.dribbblesimpleapp.listener.ShotClickListener;

/**
 * Created by Rafael on 10/06/2015.
 */
public class ShotHolder extends RecyclerView.ViewHolder {

    private ImageView mShotImage;
    private TextView mShotTitle;
    private TextView mShotCountView;

    public ShotHolder(View itemView, ShotClickListener listener) {
        super(itemView);
        itemView.setOnClickListener(listener);
        setupView(itemView);
    }

    private void setupView(View itemView) {
        setShotImage((ImageView) itemView.findViewById(R.id.image_shot));
        setShotTitle((TextView) itemView.findViewById(R.id.label_title));
        setShotCountView((TextView) itemView.findViewById(R.id.label_count));
    }

    public ImageView getShotImage() {
        return mShotImage;
    }

    public void setShotImage(ImageView shotImage) {
        this.mShotImage = shotImage;
    }

    public TextView getShotTitle() {
        return mShotTitle;
    }

    public void setShotTitle(TextView shotTitle) {
        this.mShotTitle = shotTitle;
    }

    public TextView getShotCountView() {
        return mShotCountView;
    }

    public void setShotCountView(TextView shotCountView) {
        this.mShotCountView = shotCountView;
    }

    public void setShot(Shot shot) {
        this.itemView.setTag(shot);
    }
}
