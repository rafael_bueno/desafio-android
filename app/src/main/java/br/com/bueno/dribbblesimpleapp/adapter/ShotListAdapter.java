package br.com.bueno.dribbblesimpleapp.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import br.com.bueno.dribbblesimpleapp.R;
import br.com.bueno.dribbblesimpleapp.component.Shot;
import br.com.bueno.dribbblesimpleapp.listener.ShotClickListener;

/**
 * Created by Rafael on 10/06/2015.
 */
public class ShotListAdapter extends RecyclerView.Adapter<ShotHolder> {

    private List<Shot> mItem;
    private ShotClickListener mListener;
    private Context mContext;

    public ShotListAdapter(Context context) {
        super();
        this.mContext = context;
        mListener = new ShotClickListener(context);
    }

    @Override
    public ShotHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.shot_list_item, parent, false);

        ShotHolder vh = new ShotHolder(v, mListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(ShotHolder holder, int position) {
        Shot shot = mItem.get(position);
        holder.setShot(shot);
        holder.getShotCountView().setText(String.valueOf(shot.getViewCount()));
        holder.getShotTitle().setText(shot.getTitle());

        Picasso.with(mContext).load(shot.getImageUrl())
                .placeholder(R.drawable.loading_screen)
                .error(R.drawable.error_screen).into(holder.getShotImage());

    }


    public void newPage(List<Shot> shots) {
        if (mItem == null)
            mItem = new ArrayList<Shot>();

        mItem.addAll(shots);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {

        if (mItem == null) {
            return 0;
        }

        return mItem.size();
    }
}
