package br.com.bueno.dribbblesimpleapp.component;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Rafael on 10/06/2015.
 */
public class Player implements Serializable {

    @SerializedName("name")
    private String mName;
    @SerializedName("avatar_url")
    private String mAvatarUrl;
    @SerializedName("url")
    private String mUrl;

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getAvatarUrl() {
        return mAvatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.mAvatarUrl = avatarUrl;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        this.mUrl = url;
    }
}
