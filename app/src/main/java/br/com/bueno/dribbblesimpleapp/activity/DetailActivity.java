package br.com.bueno.dribbblesimpleapp.activity;

import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import br.com.bueno.dribbblesimpleapp.R;
import br.com.bueno.dribbblesimpleapp.component.Shot;

/**
 * Created by Rafael on 10/06/2015.
 */
@EActivity(R.layout.activity_detail)
public class DetailActivity extends BaseActivity {

    @ViewById(R.id.name_detail)
    TextView mName;
    @ViewById(R.id.txt_descrption)
    TextView mDescription;
    @ViewById(R.id.label_title)
    TextView mTitleDribbble;
    @ViewById(R.id.label_count)
    TextView mViewCount;
    @ViewById(R.id.image_shot)
    ImageView mDribbbleImageView;
    @ViewById(R.id.perfil_image)
    ImageView mAvatar;

    Shot mShot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mShot = (Shot) getIntent().getSerializableExtra("SHOT");
    }

    @AfterViews
    void configureViews() {
        mName.setText(mShot.getPlayer().getName());
        mDescription.setText(Html.fromHtml(mShot.getDescription() == null ? "" : mShot.getDescription()));
        mDescription.setMovementMethod(LinkMovementMethod.getInstance());
        mTitleDribbble.setText(mShot.getTitle());
        mViewCount.setText(String.valueOf(mShot.getViewCount()));

        Picasso.with(this).load(mShot.getImageUrl()).placeholder(R.drawable.loading_screen).
                error(R.drawable.error_screen).into(mDribbbleImageView);
        Picasso.with(this).load(mShot.getPlayer().getAvatarUrl()).
                placeholder(R.drawable.loading_screen).error(R.drawable.ic_error).into(mAvatar);
    }
}