package br.com.bueno.dribbblesimpleapp.component;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Rafael on 10/06/2015.
 */
public class Shot implements Serializable {

    @SerializedName("id")
    private int mId;
    @SerializedName("title")
    private String mTitle;
    @SerializedName("image_url")
    private String mImageUrl;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("views_count")
    private int mViewCount;
    @SerializedName("player")
    private Player mPlayer;

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.mImageUrl = imageUrl;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }

    public int getViewCount() {
        return mViewCount;
    }

    public void setViewCount(int viewCount) {
        this.mViewCount = viewCount;
    }

    public Player getPlayer() {
        return mPlayer;
    }

    public void setPlayer(Player player) {
        this.mPlayer = player;
    }
}
