package br.com.bueno.dribbblesimpleapp.activity;

import android.app.ProgressDialog;
import android.support.v7.app.ActionBarActivity;

import br.com.bueno.dribbblesimpleapp.R;

/**
 * Created by Rafael on 10/06/2015.
 */
public class BaseActivity extends ActionBarActivity {

    private ProgressDialog mDialog;

    public void showProgressDialog() {

        dismissProgressDialog();

        if (!isFinishing()) {
            mDialog = new ProgressDialog(this);
            mDialog.setCancelable(false);
            mDialog.setTitle(getString(R.string.wait));
            mDialog.setMessage(getString(R.string.loading));
            mDialog.show();
        }
    }

    public void dismissProgressDialog() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.cancel();
            mDialog.dismiss();
            mDialog = null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if ((mDialog != null) && mDialog.isShowing())
            mDialog.dismiss();

        mDialog = null;
    }
}
