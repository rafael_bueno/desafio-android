package br.com.bueno.dribbblesimpleapp;

import android.test.ActivityInstrumentationTestCase2;

import com.robotium.solo.Solo;

import br.com.bueno.dribbblesimpleapp.activity.DetailActivity_;
import br.com.bueno.dribbblesimpleapp.activity.DribbbleActivity_;

/**
 * Created by Rafael on 11/06/2015.
 */
public class DribbbleBlackBoxTest extends ActivityInstrumentationTestCase2<DribbbleActivity_> {

    private Solo mSolo;

    public DribbbleBlackBoxTest() {
        super(DribbbleActivity_.class);
    }

    @Override
    protected void setUp() throws Exception {
        mSolo = new Solo(getInstrumentation(), getActivity());
    }

    @Override
    public void tearDown() throws Exception {
        mSolo.finishOpenedActivities();
        super.tearDown();
    }

    public void testActivityAppear() {
        mSolo.assertCurrentActivity("Wrong Activity!", DribbbleActivity_.class);
    }

    public void testActionBarButtons() {
        mSolo.clickOnActionBarItem(R.id.action_refresh);
        mSolo.clickOnActionBarItem(R.id.action_about);
    }

    public void testDetailShot() {
        mSolo.clickInRecyclerView(1);
        mSolo.assertCurrentActivity("Is not Detail activity!!", DetailActivity_.class);
    }
}